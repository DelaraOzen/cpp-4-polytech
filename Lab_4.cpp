﻿#include <iostream>
using namespace std;
void print_to_console(int a, int b)
{
	cout « "Сумма главной диагонали равна " « a « endl;
	cout « "Сумма побочной диагонали равна " « b « endl;
}
int sum_of_num(int n)
{
	int sum = 0;
	while (n > 0)
	{
		sum += n % 10;
		n /= 10;
	}
	return sum;
}
int main()
{
	setlocale(LC_ALL, "rus");
	int arr[5][5] = {
	{ 1, 1, 1, 1, 15 },
	{ 1, 1, 1, 15, 2 },
	{ 1, 1, 15, 2, 2 },
	{ 1, 15, 2, 2, 2 },
	{ 15, 2, 2, 2, 2 }
	};
	int sum_main;
	int sum;
	for (int i = 0; i < 5; i++)
	{
		for (int j = 0; j < 5; j++)
		{
			cout « arr[i][j] « "\t";
		}
		cout « endl;
	}
	sum = arr[0][4] + arr[1][3] + arr[2][2] + arr[3][1] + arr[4][0];
	sum_main = arr[0][0] + arr[1][1] + arr[2][2] + arr[3][3] + arr[4][4];
	print_to_console(sum_main, sum);
	cout « endl;
	int arry[5] = { 0 };
	int bottle;
	for (int i = 0; i < 5; i++)
	{
		cout « "Введите элемент масива номер " « i + 1 « " ";
		cin » arry[i];
	}
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			if (sum_of_num(arry[j]) < sum_of_num(arry[j + 1]))
			{
				bottle = arry[j];
				arry[j] = arry[j + 1];
				arry[j + 1] = bottle;
			}
		}
	}
	for (int i = 0; i < 5; i++)
	{
		cout « arry[i] « "\t" « endl;
	}
	return 0;
}